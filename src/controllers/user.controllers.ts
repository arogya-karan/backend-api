import jwt from "jsonwebtoken";
import { config } from "dotenv";
config();
// const Pusher = require("pusher");

import { UserModel } from "../model/user.model";
import { Request, Response, NextFunction } from "express";
import ProductModel from "../model/product.model";
import SellerModel from "../model/seller.model";

// const pusher = new Pusher({
//   appId: process.env.PUSHER_APP_ID?.toString(),
//   key: process.env.PUSHER_KEY?.toString(),
//   secret: process.env.PUSHER_SECRET?.toString(),
//   cluster: process.env.PUSHER_CLUSTER?.toString(),
//   encrypted: true,
// });

const JWT_SECRET = process.env.JWT_SECRET!.toString();

export const getAllUsers = (req: any, res: Response, next: NextFunction) => {
  jwt.verify(req.token, JWT_SECRET, (err: any, decoded: any) => {
    console.log(decoded);

    if (!err) {
      UserModel.find({})
        .then((users) => {
          if (users) {
            res.json({
              success: true,
              users,
            });
          } else {
            res.status(400).json({
              success: false,
              err: "Users Don't exist!!",
            });
          }
        })
        .catch((err) => {
          res.status(400).json({
            success: false,
            err,
          });
        });
    } else {
      res.status(403).json({
        success: false,
        err,
      });
    }
  });
};

// export const postBuyProduct = (req: any, res: Response, next: NextFunction) => {
//   jwt.verify(req.token, JWT_SECRET, (err: any, payload: any) => {
//     if (!err) {
//       let productCode = req.body.productCode;
//       let quantity = req.body.quantity;

//       ProductModel.findOne({ productCode: productCode })
//         .then((product) => {
//           if (product) {
//             console.log(product.sellerId);
//             if (product.availablity) {
//               SellerModel.findOne({ uid: product.sellerId })
//                 .then((seller) => {
//                   UserModel.findById(payload.user._id)
//                     .then((user) => {
//                       user?.requested.push({
//                         sid: seller?._id,
//                         quantity: quantity,
//                         pid: product._id,
//                         date: Date.now().toString(),
//                       });

//                       console.log("check 88");

//                       user
//                         ?.save()
//                         .then((user) => {
//                           console.log(seller);

//                           seller?.requested.push({
//                             uid: payload.user._id,
//                             pid: product._id,
//                             quantity: quantity,
//                             date: Date.now().toString(),
//                           });

//                           console.log("Check!! 100");

//                           seller
//                             ?.save()
//                             .then((seller) => {
//                               console.log("check 105");
//                               jwt.sign(
//                                 { user },
//                                 JWT_SECRET,
//                                 (err: any, token: any) => {
//                                   if (!err) {
//                                     console.log("before pusher");

//                                     pusher.trigger(
//                                       "user-buy-request",
//                                       "requested-product",
//                                       {
//                                         uid: user._id,
//                                         sid: seller.uid,
//                                         quantity: quantity,
//                                         pid: product._id,
//                                       }
//                                     );

//                                     console.log("after pusher");

//                                     res.json({
//                                       success: true,
//                                       msg: "Product successfully requested!!",
//                                       token,
//                                     });
//                                   } else {
//                                     res.status(400).json({
//                                       success: false,
//                                       err,
//                                     });
//                                   }
//                                 }
//                               );
//                             })
//                             .catch((err) => {
//                               res.status(400).json({
//                                 success: false,
//                                 err,
//                               });
//                             });
//                         })
//                         .catch((err) => {
//                           res.status(400).json({
//                             success: false,
//                             err,
//                           });
//                         });
//                     })
//                     .catch((err) => {
//                       res.status(400).json({
//                         success: false,
//                         err,
//                       });
//                     });
//                 })
//                 .catch((err) => {
//                   res.status(400).json({
//                     success: false,
//                     err,
//                   });
//                 });
//             } else {
//               res.status(400).json({
//                 success: false,
//                 msg: "Product Unavailable!!",
//               });
//             }
//           } else {
//             res.status(400).json({
//               success: false,
//               msg: "Product not found!",
//             });
//           }
//         })
//         .catch((err) => {
//           res.status(400).json({
//             success: false,
//             err,
//           });
//         });
//     } else {
//       res.status(503).json({
//         success: false,
//         err,
//       });
//     }
//   });
// };

export const getUserById = (req: any, res: Response, next: NextFunction) => {
  let uid = req.params.uid;
  jwt.verify(req.token, JWT_SECRET, (err: any, decoded: any) => {
    if (!err) {
      UserModel.findById(uid)
        .then((user) => {
          res.json({
            success: true,
            user,
          });
        })
        .catch((err) => {
          res.status(400).json({
            success: false,
            err,
          });
        });
    } else {
      res.status(503).json({
        success: false,
        err,
      });
    }
  });
};

// export const getRequestedProducts = (
//   req: any,
//   res: Response,
//   next: NextFunction
// ) => {
//   jwt.verify(req.token, JWT_SECRET, (err: any, payload: any) => {
//     if (!err) {
//       UserModel.findById({ uid: payload.user._id })
//         .then((user) => {
//           res.json({
//             success: true,
//             requested: user?.requested,
//           });
//         })
//         .catch((err) => {
//           res.status(400).json({
//             success: false,
//             msg: "User Not Found",
//             err,
//           });
//         });
//     } else {
//       res.status(503).json({
//         success: false,
//         msg: "Token Invalid!!",
//         err,
//       });
//     }
//   });
// };
