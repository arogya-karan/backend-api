import { Request, Response, NextFunction } from "express";
import jwt from "jsonwebtoken";
import bcrypt, { hash } from "bcrypt";
import nodemailer from "nodemailer";
import { config } from "dotenv";
config();
const nodemailerSendgrid = require("nodemailer-sendgrid");

import { UserModel, IUser } from "../model/user.model";

const JWT_SECRET = process.env.JWT_SECRET!.toString();


const transport = nodemailer.createTransport(
  nodemailerSendgrid({
    apiKey: process.env.SENDGRID_API_KEY,
  })
);

export const postAddUnverifiedUsers = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  let regUser = req.body;

  UserModel.findOne({ name: regUser.sellerName })
    .then((prevUser) => {
      if (prevUser) {
        return res.status(401).json({
          success: false,
          err: "User already exists",
        });
      }

      let newUser = new UserModel({
        name: regUser.sellername,
        phone: regUser.sellerphone,
        email: regUser.selleremail,
        address: regUser.selleraddress,
        isVerified: false,
        isSeller: true,
      });

      newUser
        .save()
        .then(user => {
          res.json({
            success: true,
            user
          })
        })
        .catch((err) => {
          res.status(400).json({
            success: false,
            err,
          });
        });
    })
    .catch((err) => {
      res.status(400).json({
        success: false,
        err,
      });
    });
};

export const postRegistration = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  let regUser = req.body;

  UserModel.findOne({ email: regUser.email })
    .then((user) => {
      if (user) {
        res.status(401).json({
          success: false,
          err: "User already exists",
        });
      } else {
        bcrypt.hash(regUser.password, 12, (err, hash) => {
          if (!err) {
            let newUser = new UserModel({
              name: regUser.name,
              phone: regUser.phone,
              email: regUser.email,
              password: hash,
              address: regUser.address,
              geometry: {
                coordinates: [
                  parseFloat(regUser.long),
                  parseFloat(regUser.lat),
                ],
              },
              isVerified: true,
            });

            newUser
              .save()
              .then((user) => {
                // console.log(user);

                jwt.sign({ user }, JWT_SECRET, (err: any, token: any) => {
                  if (!err) {
                    transport.sendMail(
                      {
                        from: "support@kalamanthan.in",
                        to: user.email,
                        subject: "Registration Confirmation",
                        html: `<p> Please open this link in mobile browser!!  <a href="http://46.101.189.145/api/auth/register/${token}">Open This Link to Validate Registration</a></p>`,
                      },
                      (err, info) => {
                        if (!err) {
                          res.json({
                            success: true,
                            msg: "Email sent!!",
                            info,
                          });
                        } else {
                          res.status(400).json({
                            success: false,
                            err,
                          });
                        }
                      }
                    );
                  } else {
                    res.status(400).json({
                      success: false,
                      err: "Error!! Cannot authorize the registration!!",
                    });
                  }
                });
              })
              .catch((err) => {
                res.status(400).json({
                  success: false,
                  err,
                });
              });
          } else {
            res.status(400).json({
              success: false,
              err,
            });
          }
        });
      }
    })
    .catch((err) => {
      res.status(400).json({
        success: false,
        err,
      });
    });
};

export const getRegiter = (req: Request, res: Response, next: NextFunction) => {
  res.send(
    "<h1> Please open this link in mobile browsers with the app installed!! </h1>"
  );
};

export const postLogin = (req: Request, res: Response, next: NextFunction) => {
  let reqUser = req.body;

  UserModel.findOne({ email: reqUser.email })
    .then((user) => {
      if (!user) {
        res.status(401).json({
          success: false,
          err: "User doesn't exist!! Please register first",
        });
      } else {
        bcrypt.compare(reqUser.password, user.password, (err, same) => {
          if (!err) {
            if (same) {
              jwt.sign({ user }, JWT_SECRET, (err: any, token: any) => {
                if (!err) {
                  res.json({
                    success: true,
                    token,
                  });
                } else {
                  res.status(400).json({
                    success: false,
                    err: "Error!! Cannot authorize Login!!",
                  });
                }
              });
            } else {
              res.status(400).json({
                success: false,
                err: "Passwords don't match",
              });
            }
          } else {
            res.status(400).json({
              success: false,
              err,
            });
          }
        });
      }
    })
    .catch((err) => {
      res.status(400).json({
        success: false,
        err,
      });
    });
};

export const putChangePassword = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  let body = req.body;

  UserModel.findOne({ email: body.email })
    .then((user) => {
      if (user) {
        bcrypt.compare(body.oldPassword, user.password, (err, same) => {
          if (!err) {
            if (same) {
              bcrypt.hash(body.newPassword, 12, (err, hash) => {
                if (!err) {
                  user.password = hash;

                  user
                    .save()
                    .then((newUser) => {
                      jwt.sign(
                        { user: newUser },
                        JWT_SECRET,
                        (err: any, token: any) => {
                          if (!err) {
                            res.json({
                              success: true,
                              token,
                            });
                          } else {
                            res.status(400).json({
                              success: false,
                              err: "Error!! Cannot authorize Login!!",
                            });
                          }
                        }
                      );
                    })
                    .catch((err) => {
                      res.status(400).json({
                        success: false,
                        err,
                      });
                    });
                } else {
                  res.status(400).json({
                    success: false,
                    err,
                  });
                }
              });
            } else {
              res.status(400).json({
                success: false,
                err: "Old Password is not correct!!",
              });
            }
          } else {
            res.status(400).json({
              success: false,
              err,
            });
          }
        });
      } else {
        res.status(400).json({
          success: false,
          err: "User not found!!",
        });
      }
    })
    .catch((err) => {
      res.status(400).json({
        success: false,
        err,
      });
    });
};

export const postForgotPassword = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  let email = req.body.email;

  let buff = new Buffer(email);
  let encodedEmail = buff.toString("base64");

  UserModel.findOne({ email: email })
    .then((user) => {
      if (user) {
        transport.sendMail(
          {
            from: "support@kalamanthan.in",
            to: user.email,
            subject: "Change Password",
            html: `<p> Please open this link in mobile browser!!  <a href="http://46.101.189.145/api/auth/forgotpassword/${encodedEmail}">Open This Link to Change Password</a></p>`,
          },
          (err, info) => {
            if (!err) {
              res.json({
                success: true,
                msg: "Email sent!!",
                info,
              });
            } else {
              res.json({
                success: false,
                msg: "Email sent!!",
              });
            }
          }
        );
      } else {
        res.status(400).json({
          success: false,
          err: "User not found!!",
        });
      }
    })
    .catch((err) => {
      res.status(400).json({
        success: false,
        err,
      });
    });
};

export const putForgotPassword = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  let body = req.body;

  UserModel.findOne({ email: body.email })
    .then((user) => {
      if (user) {
        bcrypt.hash(body.password, 12, (err, hash) => {
          if (!err) {
            user.password = hash;

            user
              .save()
              .then((newUser) => {
                res.json({
                  success: true,
                  msg: "Password Successfully Changed!!",
                });
              })
              .catch((err) => {
                res.status(400).json({
                  success: false,
                  err,
                });
              });
          } else {
            res.status(400).json({
              success: false,
              err,
            });
          }
        });
      } else {
        res.status(400).json({
          success: false,
          err: "User not found!!",
        });
      }
    })
    .catch((err) => {
      res.status(400).json({
        success: false,
        err,
      });
    });
};

export const getForgotPassword = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  res.send(
    "<h1> Please open this link in mobile browsers with the app installed!! </h1>"
  );
};
