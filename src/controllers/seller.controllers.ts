import { Request, Response, NextFunction } from "express";
import jwt, { JwtHeader } from "jsonwebtoken";
import { config } from "dotenv";
config();
import _ from "lodash";
// import axios from "axios";
// const Pusher = require("pusher");

import SellerModel from "../model/seller.model";
import ProductModel from "../model/product.model";
import { UserModel } from "../model/user.model";
import sellerRouter from "../routes/seller.routes";

// const pusher = new Pusher({
//   appId: process.env.PUSHER_APP_ID?.toString(),
//   key: process.env.PUSHER_KEY?.toString(),
//   secret: process.env.PUSHER_SECRET?.toString(),
//   cluster: process.env.PUSHER_CLUSTER?.toString(),
//   encrypted: true,
// });

const JWT_SECRET = process.env.JWT_SECRET!.toString();

export const postAddUnverifiedSeller = (
  req: any,
  res: Response,
  next: NextFunction
) => {
  let regSeller = req.body;

  SellerModel.findOne({ sellerName: regSeller.sellername }).then(
    async (prevSeller) => {
      if (prevSeller) {
        return res.status(401).json({
          success: false,
          err: "Seller already exists",
        });
      }

      let userSeller = await UserModel.findOne({ name: regSeller.sellername });

      let newSeller = new SellerModel({
        uid: userSeller?._id,
        sellerName: regSeller.sellername,
        sellerPhone: regSeller.sellerphone,
        sellerEmail: regSeller.selleremail,
        sellerAddress: regSeller.selleraddress,
        geometry: {
          coordinates: [parseFloat(regSeller.long), parseFloat(regSeller.lat)],
        },
        isBusiness: regSeller.isBusiness,
        isVerifiedSeller: false,
      });

      newSeller
        .save()
        .then((seller) => {
          res.json({
            success: true,
            seller,
          });
        })
        .catch((err) => {
          res.status(403).json({
            success: false,
            err,
          });
        });
    }
  );
};

export const postAddSeller = (req: any, res: Response, next: NextFunction) => {
  let regSeller = req.body;

  jwt.verify(req.token, JWT_SECRET, (err: any, payload: any) => {
    if (!err) {
      if (regSeller.isBusiness) {
        let newSeller = new SellerModel({
          uid: payload.user._id,
          sellerName: regSeller.name,
          sellerPhone: regSeller.phone,
          sellerEmail: regSeller.email,
          sellerAddress: regSeller.address,
          geometry: {
            coordinates: [
              parseFloat(regSeller.long),
              parseFloat(regSeller.lat),
            ],
          },
          isBusiness: true,
          isVerifiedSeller: true,
        });

        newSeller
          .save()
          .then((seller) => {
            UserModel.findById(payload.user._id)
              .then((user) => {
                user!.isSeller = true;

                user
                  ?.save()
                  .then((newUser) => {
                    jwt.sign(
                      { user: newUser },
                      JWT_SECRET,
                      (err: any, token: any) => {
                        if (!err) {
                          res.json({
                            success: true,
                            token,
                            seller,
                          });
                        } else {
                          console.log("Error 8");
                          res.status(403).json({
                            success: false,
                            err,
                          });
                        }
                      }
                    );
                  })
                  .catch((err) => {
                    console.log("Error 1");
                    res.status(403).json({
                      success: false,
                      err,
                    });
                  });
              })
              .catch((err) => {
                console.log("Error 2");
                res.status(403).json({
                  success: false,
                  err,
                });
              });
          })
          .catch((err) => {
            console.log("Error 3");
            res.status(403).json({
              success: false,
              err,
            });
          });
      } else {
        let address;
        if (regSeller.isNewAddress) {
          address = regSeller.address;
        } else {
          address = payload.user.address;
        }

        let newSeller = new SellerModel({
          uid: payload.user._id,
          sellerName: payload.user.name,
          sellerPhone: payload.user.phone,
          sellerEmail: payload.user.email,
          sellerAddress: address,
          isBusiness: false,
          geometry: {
            coordinates: [
              parseFloat(regSeller.long),
              parseFloat(regSeller.lat),
            ],
          },
          isVerifiedSeller: true,
        });

        newSeller
          .save()
          .then((seller) => {
            UserModel.findById(payload.user._id)
              .then((user) => {
                user!.isSeller = true;

                user
                  ?.save()
                  .then((newUser) => {
                    jwt.sign(
                      { user: newUser },
                      JWT_SECRET,
                      (err: any, token: any) => {
                        if (!err) {
                          res.json({
                            success: true,
                            token,
                            seller,
                          });
                        } else {
                          console.log("Error 9");
                          res.status(403).json({
                            success: false,
                            err,
                          });
                        }
                      }
                    );
                  })
                  .catch((err) => {
                    console.log("Error 4");
                    res.status(403).json({
                      success: false,
                      err,
                    });
                  });
              })
              .catch((err) => {
                console.log("Error 5");
                res.status(403).json({
                  success: false,
                  err,
                });
              });
          })
          .catch((err) => {
            console.log("Error 6");
            res.status(403).json({
              success: false,
              err,
            });
          });
      }
    } else {
      console.log("Error 7");
      res.status(403).json({
        success: false,
        err,
      });
    }
  });
};

export const postVerifySeller = (
  req: any,
  res: Response,
  next: NextFunction
) => {};

export const getAllProducts = (req: any, res: Response, next: NextFunction) => {
  let productArray: string[] | undefined = [];

  jwt.verify(req.token, JWT_SECRET, (err: any, payload: any) => {
    if (!err) {
      if (payload.user.isSeller) {
        SellerModel.findOne({ uid: payload.user._id })
          .then((seller) => {
            ProductModel.find({ _id: { $in: seller?.productList } })
              .then((products) => {
                res.json({
                  success: true,
                  products,
                });
              })
              .catch((err) => {
                res.status(400).json({
                  success: false,
                  msg: "Products Not Found",
                  err,
                });
              });
          })
          .catch((err) => {
            res.status(400).json({
              success: false,
              msg: "Seller Not Found!!",
              err,
            });
          });
      } else {
        res.status(400).json({
          success: false,
          msg: "User is not a Seller!!",
        });
      }
    } else {
      res.status(503).json({
        success: false,
        msg: "Invalid Token!!",
        err,
      });
    }
  });
};

export const getAllProductsBySellerId = (
  req: any,
  res: Response,
  next: NextFunction
) => {
  let productArray: string[] | undefined = [];

  jwt.verify(req.token, JWT_SECRET, (err: any, payload: any) => {
    if (!err) {
      SellerModel.findById(req.params.id)
        .then((seller) => {
          ProductModel.find({ _id: { $in: seller?.productList } })
            .then((products) => {
              res.json({
                success: true,
                products,
              });
            })
            .catch((err) => {
              res.status(400).json({
                success: false,
                msg: "Products Not Found",
                err,
              });
            });
        })
        .catch((err) => {
          res.status(400).json({
            success: false,
            msg: "Seller Not Found!!",
            err,
          });
        });
    } else {
      res.status(503).json({
        success: false,
        msg: "Invalid Token!!",
        err,
      });
    }
  });
};

export const getAllProductsBySellerIdWithoutLogin = (
  req: any,
  res: Response,
  next: NextFunction
) => {
  let productArray: string[] | undefined = [];

  SellerModel.findById(req.params.id)
    .then((seller) => {
      ProductModel.find({ _id: { $in: seller?.productList } })
        .then((products) => {
          res.json({
            success: true,
            products,
          });
        })
        .catch((err) => {
          res.status(400).json({
            success: false,
            msg: "Products Not Found",
            err,
          });
        });
    })
    .catch((err) => {
      res.status(400).json({
        success: false,
        msg: "Seller Not Found!!",
        err,
      });
    });
};

export const getSellerNearby = (
  req: any,
  res: Response,
  next: NextFunction
) => {
  jwt.verify(req.token, JWT_SECRET, (err: any, payload: any) => {
    if (!err) {
      SellerModel.createIndexes({
        point: "2dsphere",
        spherical: true
      } as any);

      SellerModel.aggregate()
        .near({
          near: {
            type: "Point",
            coordinates: [
              parseFloat(req.query.long),
              parseFloat(req.query.lat),
            ],
          },
          maxDistance: parseInt(req.query.radius)*1000,
          spherical: true,
          distanceField: "dis",
        })
        .then((sellersAll) => {
          const sellers = sellersAll.map((item) => {
            return {
              _id: item._id,
              uid: item.uid,
              sellerName: item.sellerName,
              geometry: item.geometry,
              isBusiness: item.isBusiness,
              dis: item.dis
            };
          });
          res.json({
            success: true,
            sellers,
          });
        })
        .catch((err) => {
          res.status(400).json({
            success: false,
            msg: "Error in finding nearest Seller",
            err,
          });
        });
    } else {
      res.status(503).json({
        success: false,
        msg: "Token Invalid!!",
        err,
      });
    }
  });
};

export const getSellerNearbyWithoutLogin = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  SellerModel.createIndexes({
    point: "2dsphere",
    spherical: true
  } as any);

  SellerModel.aggregate()
    .near({
      near: {
        type: "Point",
        coordinates: [
          parseFloat(req.query.long.toString()),
          parseFloat(req.query.lat.toString()),
        ],
      },
      spherical: true,
      distanceField: "dis",
    })
    .then((sellersAll) => {
      const sellers = sellersAll.map((item) => {
        return {
          _id: item._id,
          uid: item.uid,
          sellerName: item.sellerName,
          geometry: item.geometry,
          isBusiness: item.isBusiness,
        };
      });
      res.json({
        success: true,
        sellers,
      });
    })
    .catch((err) => {
      res.status(400).json({
        success: false,
        msg: "Error in finding nearest Seller",
        err,
      });
    });
};

export const getSellerById = (req: any, res: Response, next: NextFunction) => {
  jwt.verify(req.token, JWT_SECRET, (err: any, payload: any) => {
    if (!err) {
      SellerModel.findById(req.params.id)
        .then((seller) => {
          res.json({
            success: true,
            seller,
          });
        })
        .catch((err) => {
          res.status(400).json({
            success: false,
            msg: "Seller Not Found",
            err,
          });
        });
    } else {
      res.status(503).json({
        success: false,
        msg: "Token Invalid!!",
        err,
      });
    }
  });
};

export const getSellerByUid = (req: any, res: Response, next: NextFunction) => {
  jwt.verify(req.token, JWT_SECRET, (err: any, payload: any) => {
    if (!err) {
      SellerModel.findOne({ uid: req.params.uid })
        .then((seller) => {
          res.json({
            success: true,
            seller,
          });
        })
        .catch((err) => {
          res.status(400).json({
            success: false,
            msg: "Seller Not Found",
            err,
          });
        });
    } else {
      res.status(503).json({
        success: false,
        msg: "Token Invalid!!",
        err,
      });
    }
  });
};
