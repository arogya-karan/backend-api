import e, { Response, NextFunction } from "express";
import jwt from "jsonwebtoken";
import { UserModel } from "../model/user.model";
import ProductModel from "../model/product.model";
import SellerModel from "../model/seller.model";
import { UTransactModel } from "../model/userTransaction.model";
import { STransactModel } from "../model/sellerTransaction.model";
// const Pusher = require("pusher");
import { config } from "dotenv";
config();

const JWT_SECRET = process.env.JWT_SECRET!.toString();

// const pusher = new Pusher({
//   appId: process.env.PUSHER_APP_ID?.toString(),
//   key: process.env.PUSHER_KEY?.toString(),
//   secret: process.env.PUSHER_SECRET?.toString(),
//   cluster: process.env.PUSHER_CLUSTER?.toString(),
//   encrypted: true,
// });

export const postUserRequest = (
  req: any,
  res: Response,
  next: NextFunction
) => {
  let purchase = req.body;

  jwt.verify(req.token, JWT_SECRET, (err: any, payload: any) => {
    if (!err) {
      let dateOfRequest = Date.now().toString();
      UserModel.findById(payload.user._id)
        .then((user) => {
          if (user) {
            ProductModel.findById(purchase.pid)
              .then((product) => {
                if (product) {
                  console.log(product.sellerId);

                  SellerModel.findOne({ uid: product.sellerId })
                    .then((seller) => {
                      if (seller) {
                        UTransactModel.findOne({ uid: user._id })
                          .then((trans) => {
                            if (trans) {
                              trans.requested.push({
                                quantity: purchase.quantity,
                                pid: product._id,
                                dateOfRequest,
                                sid: seller._id,
                              });

                              trans
                                .save()
                                .then((uTrans) => {
                                  STransactModel.findOne({ sid: seller._id })
                                    .then((strans) => {
                                      if (strans) {
                                        strans.requested.push({
                                          quantity: purchase.quantity,
                                          pid: product._id,
                                          dateOfRequest,
                                          uid: user._id,
                                        });

                                        strans
                                          .save()
                                          .then((trans) => {
                                            // pusher.trigger(
                                            //   "user-buy-request",
                                            //   "requested-product",
                                            //   {
                                            //     uid: user._id,
                                            //     sid: seller.uid,
                                            //     quantity: purchase.quantity,
                                            //     pid: product._id,
                                            //   }
                                            // );
                                            res.json({
                                              success: true,
                                              msg: "Product Requested!!",
                                            });
                                          })
                                          .catch((err) => {
                                            res.status(400).json({
                                              success: false,
                                              err,
                                            });
                                          });
                                      } else {
                                        const newSTrans = new STransactModel({
                                          sid: seller._id,
                                          requested: [
                                            {
                                              quantity: purchase.quantity,
                                              pid: product._id,
                                              dateOfRequest,
                                              uid: user._id,
                                            },
                                          ],
                                          transactions: [],
                                        });

                                        newSTrans
                                          .save()
                                          .then((trans) => {
                                            // pusher.trigger(
                                            //   "user-buy-request",
                                            //   "requested-product",
                                            //   {
                                            //     uid: user._id,
                                            //     sid: seller.uid,
                                            //     quantity: purchase.quantity,
                                            //     pid: product._id,
                                            //   }
                                            // );
                                            res.json({
                                              success: true,
                                              msg: "Product Requested!!",
                                            });
                                          })
                                          .catch((err) => {
                                            res.status(400).json({
                                              success: false,
                                              err,
                                            });
                                          });
                                      }
                                    })
                                    .catch((err) => {
                                      res.status(400).json({
                                        success: false,
                                        err,
                                      });
                                    });
                                })
                                .catch((err) => {
                                  res.status(400).json({
                                    success: false,
                                    err,
                                  });
                                });
                            } else {
                              let newTrans = new UTransactModel({
                                uid: user._id,
                                requested: [
                                  {
                                    quantity: purchase.quantity,
                                    pid: product._id,
                                    dateOfRequest,
                                    sid: seller._id,
                                  },
                                ],
                                buy: [],
                              });

                              newTrans
                                .save()
                                .then((uTrans) => {
                                  STransactModel.findOne({ sid: seller._id })
                                    .then((strans) => {
                                      if (strans) {
                                        strans.requested.push({
                                          quantity: purchase.quantity,
                                          pid: product._id,
                                          dateOfRequest,
                                          uid: user._id,
                                        });

                                        strans
                                          .save()
                                          .then((trans) => {
                                            // pusher.trigger(
                                            //   "user-buy-request",
                                            //   "requested-product",
                                            //   {
                                            //     uid: user._id,
                                            //     sid: seller.uid,
                                            //     quantity: purchase.quantity,
                                            //     pid: product._id,
                                            //   }
                                            // );
                                            res.json({
                                              success: true,
                                              msg: "Product Requested!!",
                                            });
                                          })
                                          .catch((err) => {
                                            res.status(400).json({
                                              success: false,
                                              err,
                                            });
                                          });
                                      } else {
                                        const newSTrans = new STransactModel({
                                          sid: seller._id,
                                          requested: [
                                            {
                                              quantity: purchase.quantity,
                                              pid: product._id,
                                              dateOfRequest,
                                              uid: user._id,
                                            },
                                          ],
                                          transactions: [],
                                        });

                                        newSTrans
                                          .save()
                                          .then((trans) => {
                                            // pusher.trigger(
                                            //   "user-buy-request",
                                            //   "requested-product",
                                            //   {
                                            //     uid: user._id,
                                            //     sid: seller.uid,
                                            //     quantity: purchase.quantity,
                                            //     pid: product._id,
                                            //   }
                                            // );
                                            res.json({
                                              success: true,
                                              msg: "Product Requested!!",
                                            });
                                          })
                                          .catch((err) => {
                                            res.status(400).json({
                                              success: false,
                                              err,
                                            });
                                          });
                                      }
                                    })
                                    .catch((err) => {
                                      res.status(400).json({
                                        success: false,
                                        err,
                                      });
                                    });
                                })
                                .catch((err) => {
                                  res.status(400).json({
                                    success: false,
                                    err,
                                  });
                                });
                            }
                          })
                          .catch((err) => {
                            res.status(400).json({
                              success: false,
                              err,
                            });
                          });
                      } else {
                        res.status(400).json({
                          success: false,
                          msg: "Seller not found!!",
                        });
                      }
                    })
                    .catch((err) => {
                      res.status(400).json({
                        success: false,
                        err,
                      });
                    });
                } else {
                  res.status(400).json({
                    success: false,
                    msg: "Product not found!!",
                  });
                }
              })
              .catch((err) => {
                res.status(400).json({
                  success: false,
                  err,
                });
              });
          } else {
            res.status(400).json({
              success: false,
              msg: "User not found!!",
            });
          }
        })
        .catch((err) => {
          res.status(400).json({
            success: false,
            err,
          });
        });
    } else {
      res.status(503).json({
        success: false,
        err,
      });
    }
  });
};

export const postSellerRequest = (
  req: any,
  res: Response,
  next: NextFunction
) => {
  let purchase = req.body;

  jwt.verify(req.token, JWT_SECRET, (err: any, payload: any) => {
    if (!err) {
      if (payload.user.isSeller) {
        SellerModel.findOne({ uid: payload.user._id })
          .then((seller) => {
            if (seller) {
              ProductModel.findById(purchase.pid)
                .then((product) => {
                  if (product) {
                    UserModel.findById(purchase.uid)
                      .then((user) => {
                        if (user) {
                          UTransactModel.findOne({ uid: user._id })
                            .then((uTrans) => {
                              for (
                                let i = 0;
                                i < uTrans!.requested.length;
                                i++
                              ) {
                                if (
                                  uTrans!.requested[i].pid === purchase.pid &&
                                  uTrans?.requested[i].dateOfRequest ===
                                    purchase.dateOfRequest
                                ) {
                                  uTrans!.requested.splice(i, 1);
                                }
                              }

                              uTrans?.buy.push({
                                date: Date.now().toString(),
                                pid: product._id,
                                sid: seller._id,
                                finalized: purchase.isFinalised,
                                quantity: purchase.quantity,
                              });
                              uTrans
                                ?.save()
                                .then((trans) => {
                                  STransactModel.findOne({ sid: seller._id })
                                    .then((sTrans) => {
                                      console.log(sTrans);
                                      for (
                                        let i = 0;
                                        i < sTrans!.requested.length;
                                        i++
                                      ) {
                                        if (
                                          sTrans!.requested[i].pid ===
                                            purchase.pid &&
                                          sTrans?.requested[i].dateOfRequest ===
                                            purchase.dateOfRequest
                                        ) {
                                          sTrans!.requested.splice(i, 1);
                                        }
                                      }
                                      sTrans?.transactions.push({
                                        date: Date.now().toString(),
                                        pid: product._id,
                                        uid: user._id,
                                        finalized: purchase.isFinalised,
                                        quantity: purchase.quantity,
                                      });

                                      console.log(uTrans);
                                      console.log(sTrans);

                                      sTrans
                                        ?.save()
                                        .then((trans) => {
                                          // pusher.trigger(
                                          //   "seller-accept-request",
                                          //   "requested-product",
                                          //   {
                                          //     uid: user._id,
                                          //     sid: seller.uid,
                                          //     quantity: purchase.quantity,
                                          //     pid: product._id,
                                          //     finalized: purchase.isFinalised,
                                          //   }
                                          // );

                                          res.json({
                                            success: true,
                                            msg: "Product Bought!!",
                                          });
                                        })
                                        .catch((err) => {
                                          res.status(400).json({
                                            success: false,
                                            err,
                                          });
                                        });
                                    })
                                    .catch((err) => {
                                      res.status(400).json({
                                        success: false,
                                        err,
                                      });
                                    });
                                })
                                .catch((err) => {
                                  res.status(400).json({
                                    success: false,
                                    err,
                                  });
                                });
                            })
                            .catch((err) => {
                              res.status(400).json({
                                success: false,
                                err,
                              });
                            });
                        } else {
                          res.status(400).json({
                            success: false,
                            msg: "User not found!!",
                          });
                        }
                      })
                      .catch((err) => {
                        res.status(400).json({
                          success: false,
                          err,
                        });
                      });
                  } else {
                    res.status(400).json({
                      success: false,
                      msg: "Product not found!!",
                    });
                  }
                })
                .catch((err) => {
                  res.status(400).json({
                    success: false,
                    err,
                  });
                });
            } else {
              res.status(400).json({
                success: false,
                msg: "Seller not found!!",
              });
            }
          })
          .catch((err) => {
            res.status(400).json({
              success: false,
              err,
            });
          });
      } else {
        res.status(400).json({
          success: false,
          msg: "User is not a Seller!!",
        });
      }
    } else {
      res.status(503).json({
        success: false,
        err,
      });
    }
  });
};

export const getAllRequests = (req: any, res: Response, next: NextFunction) => {
  jwt.verify(req.token, JWT_SECRET, (err: any, payload: any) => {
    if (!err) {
      if (payload.user.isSeller) {
        SellerModel.findOne({ uid: payload.user._id })
          .then((seller) => {
            STransactModel.findOne({ sid: seller!._id })
              .then((sTrans) => {
                if (req.query.onlySeller) {
                  if (sTrans) {
                    res.json({
                      success: true,
                      requested: sTrans.requested,
                    });
                  } else {
                    res.status(400).json({
                      success: false,
                      msg: "No Requests made on your products!!",
                    });
                  }
                } else {
                  UTransactModel.findOne({ uid: payload.user._id })
                    .then((uTrans) => {
                      if (uTrans) {
                        res.json({
                          success: true,
                          requested: uTrans.requested,
                        });
                      } else {
                        res.status(400).json({
                          success: false,
                          msg: "No Requests made yet as a User!!",
                        });
                      }
                    })
                    .catch((err) => {
                      res.status(400).json({
                        success: false,
                        err,
                      });
                    });
                }
              })
              .catch((err) => {
                res.status(400).json({
                  success: false,
                  err,
                });
              });
          })
          .catch((err) => {
            res.status(400).json({
              success: false,
              err,
            });
          });
      } else {
        UTransactModel.findOne({ uid: payload.user._id })
          .then((uTrans) => {
            if (uTrans) {
              res.json({
                success: true,
                requested: uTrans.requested,
              });
            } else {
              res.status(400).json({
                success: false,
                msg: "No Requests made yet as a User!!",
              });
            }
          })
          .catch((err) => {
            res.status(400).json({
              success: false,
              err,
            });
          });
      }
    } else {
      res.status(503).json({
        success: false,
        err,
      });
    }
  });
};

export const getAllTransactions = (
  req: any,
  res: Response,
  next: NextFunction
) => {
  jwt.verify(req.token, JWT_SECRET, (err: any, payload: any) => {
    if (!err) {
      if (payload.user.isSeller) {
        console.log(payload.user);

        SellerModel.findOne({ uid: payload.user._id })
          .then((seller) => {
            STransactModel.findOne({ sid: seller!._id })
              .then((sTrans) => {
                if (req.query.onlySeller) {
                  if (sTrans) {
                    res.json({
                      success: true,
                      transactions: sTrans.transactions,
                    });
                  } else {
                    res.status(400).json({
                      success: false,
                      msg: "No Transactions made on your products!!",
                    });
                  }
                } else {
                  UTransactModel.findOne({ uid: payload.user._id })
                    .then((uTrans) => {
                      if (uTrans) {
                        res.json({
                          success: true,
                          transactions: uTrans.buy,
                        });
                      } else {
                        res.status(400).json({
                          success: false,
                          msg: "No Transactions made yet as a User!!",
                        });
                      }
                    })
                    .catch((err) => {
                      res.status(400).json({
                        success: false,
                        err,
                      });
                    });
                }
              })
              .catch((err) => {
                res.status(400).json({
                  success: false,
                  err,
                });
              });
          })
          .catch((err) => {
            res.status(400).json({
              success: false,
              err,
            });
          });
      } else {
        UTransactModel.findOne({ uid: payload.user._id })
          .then((uTrans) => {
            if (uTrans) {
              res.json({
                success: true,
                transactions: uTrans.buy,
              });
            } else {
              res.status(400).json({
                success: false,
                msg: "No Transactions made yet as a User!!",
              });
            }
          })
          .catch((err) => {
            res.status(400).json({
              success: false,
              err,
            });
          });
      }
    } else {
      res.status(503).json({
        success: false,
        err,
      });
    }
  });
};
