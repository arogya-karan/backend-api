import e, { Request, Response, NextFunction } from "express";
import jwt from "jsonwebtoken";
import SellerModel from "../model/seller.model";
import ProductModel from "../model/product.model";
import _ from "lodash";
import { UserModel } from "../model/user.model";

const JWT_SECRET = process.env.JWT_SECRET!.toString();

export const postUnverifiedProduct = async (
  req: any,
  res: Response,
  next: NextFunction
) => {
  let regProduct = req.body;

  const seller = await SellerModel.findOne({
    sellerName: regProduct.sellername,
  });

  if (!seller) {
    return res.status(400).json({
      success: false,
    });
  }

  const user = await UserModel.findOne({ name: regProduct.sellername });

  let buff = new Buffer(seller!.sellerEmail);
  let buff2 = new Buffer(regProduct.name);
  let encodedEmail = buff.toString("base64");
  let encodedProductName = buff2.toString("base64");

  let productCode = `${encodedProductName}.${seller?.sellerName}.${encodedEmail}`;

  const newProduct = new ProductModel({
    productName: regProduct.name,
    productCode: productCode,
    productDesc: regProduct.desc,
    productType: regProduct.type,
    productPrice: parseFloat(regProduct.price),
    imageThumbnail: regProduct.thumbnail,
    media: regProduct.media,
    quantity: regProduct.quantity,
    availablity: regProduct.avail,
    isCertified: regProduct.isCertified,
    sellerId: user?._id,
    dateOfIssue: Date.now(),
    geometry: seller?.geometry,
  });

  newProduct
    .save()
    .then((product) => {
      seller?.productList.push(product._id.toString());

      seller
        ?.save()
        .then((result) => {
          res.json({
            success: true,
            msg: "Product Successfully added!!",
            product,
          });
        })
        .catch((err) => {
          res.status(400).json({
            success: false,
            msg: "Seller unable to add product",
            err,
          });
        });
    })
    // .catch((err) => {
    //   console.log(err.message)
    // });
};

export const postAddProduct = (req: any, res: Response, next: NextFunction) => {
  let regProduct = req.body;
  jwt.verify(req.token, JWT_SECRET, (err: any, payload: any) => {
    if (!err) {
      if (payload.user) {
        if (payload.user.isSeller) {
          console.log(payload.user._id);

          SellerModel.findOne({ uid: payload.user._id })
            .then((seller) => {
              let email = seller!.sellerEmail;

              let buff = new Buffer(email);
              let buff2 = new Buffer(regProduct.name);
              let encodedEmail = buff.toString("base64");
              let encodedProductName = buff2.toString("base64");

              let productCode = `${encodedProductName}.${seller?.sellerName}.${encodedEmail}`;

              ProductModel.findOne({ productCode: productCode })
                .then((product) => {
                  if (!product) {
                    let newProduct = new ProductModel({
                      productName: regProduct.name,
                      productCode: productCode,
                      productDesc: regProduct.desc,
                      productType: regProduct.type,
                      productTags: regProduct.tags,
                      productPrice: parseFloat(regProduct.price),
                      imageThumbnail: regProduct.thumbnail,
                      media: regProduct.media,
                      quantity: regProduct.quantity,
                      availablity: regProduct.avail,
                      isCertified: regProduct.isCertified,
                      sellerId: payload.user._id,
                      dateOfIssue: Date.now(),
                      geometry: seller?.geometry,
                    });

                    newProduct
                      .save()
                      .then((product) => {
                        seller?.productList.push(product._id.toString());

                        seller
                          ?.save()
                          .then((result) => {
                            res.json({
                              success: true,
                              msg: "Product Successfully added!!",
                              product,
                            });
                          })
                          .catch((err) => {
                            res.status(400).json({
                              success: false,
                              msg: "Seller unable to add product",
                              err,
                            });
                          });
                      })
                      .catch((err) => {
                        res.status(400).json({
                          success: false,
                          msg: "Product with same code exists!!",
                          err,
                        });
                      });
                  } else {
                    res.status(400).json({
                      success: false,
                      msg: "Product with same code exists!!",
                    });
                  }
                })
                .catch((err) => {
                  res.status(400).json({
                    success: false,
                    err,
                  });
                });
            })
            .catch((err) => {
              res.status(400).json({
                success: false,
                msg: "not this",
                err,
              });
            });
        } else {
          res.status(400).json({
            success: false,
            msg: "User is not a Seller!!",
          });
        }
      } else {
        res.status(400).json({
          success: false,
          msg: "User not found!!",
        });
      }
    } else {
      res.status(503).json({
        success: false,
        msg: "Token Invalid!!",
        err,
      });
    }
  });
};

export const getAllProducts = (req: any, res: Response, next: NextFunction) => {
  jwt.verify(req.token, JWT_SECRET, (err: any, payload: any) => {
    if (!err) {
      ProductModel.find({})
        .then((products) => {
          res.json({
            success: true,
            products,
          });
        })
        .catch((err) => {
          res.status(400).json({
            success: false,
            err,
          });
        });
    } else {
      res.status(503).json({
        success: false,
        msg: "Token Invalid!!",
        err,
      });
    }
  });
};

export const getSingleProduct = (
  req: any,
  res: Response,
  next: NextFunction
) => {
  let code = req.params.productCode;

  jwt.verify(req.token, JWT_SECRET, (err: any, payload: any) => {
    if (!err) {
      ProductModel.findOne({ productCode: code })
        .then((product) => {
          if (product) {
            res.json({
              success: true,
              product,
            });
          } else {
            res.status(400).json({
              success: false,
              msg: "No Product Found!!",
            });
          }
        })
        .catch((err) => {
          res.status(400).json({
            success: false,
            err,
          });
        });
    } else {
      res.status(503).json({
        success: false,
        msg: "Token Invalid!!",
        err,
      });
    }
  });
};

export const getSingleProductById = (
  req: any,
  res: Response,
  next: NextFunction
) => {
  let id = req.params.id;

  console.log(id);

  jwt.verify(req.token, JWT_SECRET, (err: any, payload: any) => {
    if (!err) {
      ProductModel.findById(id)
        .then((product) => {
          if (product) {
            res.json({
              success: true,
              product,
            });
          } else {
            res.status(400).json({
              success: false,
              msg: "No Product Found!!",
            });
          }
        })
        .catch((err) => {
          res.status(400).json({
            success: false,
            err,
          });
        });
    } else {
      res.status(503).json({
        success: false,
        msg: "Token Invalid!!",
        err,
      });
    }
  });
};

export const getSingleProductByIdWithoutLogin = (
  req: any,
  res: Response,
  next: NextFunction
) => {
  let id = req.params.id;

  ProductModel.findById(id)
    .then((product) => {
      if (product) {
        res.json({
          success: true,
          product,
        });
      } else {
        res.status(400).json({
          success: false,
          msg: "No Product Found!!",
        });
      }
    })
    .catch((err) => {
      res.status(400).json({
        success: false,
        err,
      });
    });
};

export const getProductNearby = (
  req: any,
  res: Response,
  next: NextFunction
) => {
  jwt.verify(req.token, JWT_SECRET, (err: any, payload: any) => {
    if (!err) {
      ProductModel.createIndexes({
        point: "2dsphere",
        spherical: true,
      } as any);

      ProductModel.aggregate()
        .near({
          near: {
            type: "Point",
            coordinates: [
              parseFloat(payload.user.geometry.coordinates[0]),
              parseFloat(payload.user.geometry.coordinates[1]),
            ],
          },
          distanceField: "dis",
          spherical: true,
        })
        .then((products) =>
          res.json({
            success: true,
            products,
          })
        )
        .catch((err) => {
          res.status(400).json({
            success: false,
            msg: "Error in finding nearest Product",
            err,
          });
        });
    } else {
      res.status(503).json({
        success: false,
        msg: "Token Invalid!!",
        err,
      });
    }
  });
};

const regexGen = (word: string, str: string) => {
  let pattern = str
    .split("")
    .map((x) => {
      return `(?=.*${x})`;
    })
    .join("");
  let regex = new RegExp(`${pattern}`, "g");
  return word.match(regex);
};

export const getSearchProduct = (
  req: any,
  res: Response,
  next: NextFunction
) => {
/**
 * thumbnail 
 * name
 * desc
 * dis
 * _id
 * sellerId
 */

  let keyword = req.query.key.substring(0, 10);

  let searchRes: any[] = [];
  let errArr: any[] = [];

  jwt.verify(req.token, JWT_SECRET, (err: any, payload: any) => {
    if (!err) {
      ProductModel.createIndexes({
        point: "2dsphere",
        spherical: true,
      } as any);

      ProductModel.aggregate()
        .near({
          near: {
            type: "Point",
            coordinates: [
              parseFloat(payload.user.geometry.coordinates[0]),
              parseFloat(payload.user.geometry.coordinates[1]),
            ],
          },
          distanceField: "dis",
          spherical: true,
        })
        .then((products) => {
          let filteredArr2: any[] = [];

          products.forEach((product) => {
            let xSub = product.productName.substring(0, 3).toLowerCase();

            if (
              product.productName.toLowerCase().includes(keyword) ||
              regexGen(xSub, keyword)
            ) {
              filteredArr2.push(product);
            }
          });

          if (filteredArr2.length > 0) {
            if (errArr.length > 0) {
              res.status(400).json({
                success: false,
                err: errArr,
              });
            } else {
              const searchRes = filteredArr2.map((item) => {
                return {
                  _id: item._id,
                  dis: item.dis,
                  sellerId: item.sellerId,
                  productDesc: item.productDesc,
                  imageThumbnail: item.imageThumbnail,
                  productName: item.productName
                };
              });              

              res.json({
                success: true,
                search: searchRes,
              });
            }
          } else {
            res.status(400).json({
              success: false,
              msg: "No match found!!",
            });
          }
        });
    } else {
      res.status(503).json({
        success: false,
        msg: "Token Invalid!!",
        err,
      });
    }
  });
};

export const putEditProduct = (req: any, res: Response, next: NextFunction) => {
  jwt.verify(req.token, JWT_SECRET, (err: any, payload: any) => {
    if (!err) {
      ProductModel.findByIdAndUpdate(req.body.pid, { $set: req.body.product })
        .then((updatedProduct) => {
          if (updatedProduct) {
            res.json({
              success: true,
              updatedProduct,
            });
          } else {
            res.status(400).json({
              success: false,
              msg: "Updated Product not Found!!",
            });
          }
        })
        .catch((err) => {
          res.status(400).json({
            success: false,
            err,
          });
        });
    } else {
      res.status(503).json({
        success: false,
        msg: "Token Invalid!!",
        err,
      });
    }
  });
};

export const deleteProduct = (req: any, res: Response, next: NextFunction) => {
  jwt.verify(req.token, JWT_SECRET, (err: any, payload: any) => {
    if (!err) {
      if (payload.user.isSeller) {
        SellerModel.findOne({ uid: payload.user._id })
          .then((seller) => {
            if (seller) {
              for (let i = 0; i < seller!.productList.length; i++) {
                if (seller?.productList[i] === req.params.id) {
                  seller!.productList.splice(i, 1);
                }
              }

              seller
                ?.save()
                .then((seller) => {
                  ProductModel.findByIdAndDelete(req.params.id)
                    .then((product) => {
                      if (product) {
                        res.json({
                          success: true,
                          msg: "Product Deleted!!",
                          deletedProduct: product,
                        });
                      } else {
                        res.status(400).json({
                          success: true,
                          msg: "Product not found!!",
                        });
                      }
                    })
                    .catch((err) => {
                      res.status(400).json({
                        success: false,
                        err,
                      });
                    });
                })
                .catch((err) => {
                  res.status(400).json({
                    success: false,
                    err,
                  });
                });
            }
          })
          .catch((err) => {
            res.status(400).json({
              success: false,
              err,
            });
          });
      } else {
        res.status(400).json({
          success: false,
          msg: "User is not a seller!!",
        });
      }
    } else {
      res.status(503).json({
        success: false,
        msg: "Token Invalid!!",
        err,
      });
    }
  });
};
