import { Request, Response, NextFunction } from "express";
import nodemailer from "nodemailer";
import { config } from "dotenv";
config();
import path from "path";
const nodemailerSendgrid = require("nodemailer-sendgrid");

const transport = nodemailer.createTransport(
  nodemailerSendgrid({
    apiKey: process.env.SENDGRID_API_KEY,
  })
);

export const getIndex = (
  req: Request,
  res: Response,
  next: NextFunction
): void => {
  res.json({
    success: true,
    msg: "API is -ve for COVID-19",
  });
};

export const postEmailSender = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  let emailData = req.body;

  if (emailData.passcode === process.env.EMAIL_PASSCODE!.toString()) {
    transport.sendMail(
      {
        from: "support@kalamanthan.in",
        to: emailData.email,
        subject: emailData.subject,
        html: `<p> ${emailData.msg} </p>`,
      },
      (err, info) => {
        if (!err) {
          res.json({
            success: true,
            msg: "Email sent!!",
            info,
          });
        } else {
          res.status(400).json({
            success: false,
            err,
          });
        }
      }
    );
  } else {
    res.status(400).json({
      success: false,
      fake: true,
    });
  }
};

export const getPrivacyPolicy = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  res.sendFile(path.join(__dirname, "..", "..", "privacypolicy.html"));
};
