import { Router } from "express";

import {
  postLogin,
  postRegistration,
  putChangePassword,
  postForgotPassword,
  putForgotPassword,
  getForgotPassword,
  getRegiter,
  postAddUnverifiedUsers,
} from "../controllers/auth.controllers";

const authRouter = Router();

// authRouter.post("/addunverified", postAddUnverifiedUsers);

authRouter.post("/login", postLogin);

authRouter.post("/register", postRegistration);

authRouter.get("/register/:code", getRegiter);

authRouter.put("/changepassword", putChangePassword);

authRouter.post("/forgotpassword", postForgotPassword);

authRouter.put("/forgotpassword", putForgotPassword);

authRouter.get("/forgotpassword/:code", getForgotPassword);

export default authRouter;
