import { Router } from "express";

import { checkToken } from "../middlewares";
import {
  postUserRequest,
  postSellerRequest,
  getAllRequests,
  getAllTransactions,
} from "../controllers/transactions.controllers";

const transRouter = Router();

transRouter.post("/request", checkToken, postUserRequest);
transRouter.post("/transaction", checkToken, postSellerRequest);
transRouter.get("/request", checkToken, getAllRequests);
transRouter.get("/transaction", checkToken, getAllTransactions);

export default transRouter;