import express from "express";

import {
  getIndex,
  postEmailSender,
  getPrivacyPolicy,
} from "../controllers/index.controlllers";

const indexRouter = express.Router();

indexRouter.get("/", getIndex);
indexRouter.post("/email", postEmailSender);
indexRouter.get("/privacypolicy", getPrivacyPolicy);

export default indexRouter;
