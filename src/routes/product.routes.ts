import { Router } from "express";

import {
  postAddProduct,
  getAllProducts,
  getSingleProduct,
  getProductNearby,
  getSearchProduct,
  getSingleProductById,
  putEditProduct,
  deleteProduct,
  postUnverifiedProduct,
} from "../controllers/product.controller";
import { checkToken } from "../middlewares";

let productRouter = Router();

productRouter.post("/addproduct", checkToken, postAddProduct);
// productRouter.post("/addunverifiedproduct", postUnverifiedProduct);
productRouter.get("/products", checkToken, getAllProducts);
productRouter.get("/products/:productCode", checkToken, getSingleProduct);
productRouter.get("/productsbyid/:id", checkToken, getSingleProductById);
productRouter.get("/getnearby", checkToken, getProductNearby);
productRouter.get("/search", checkToken, getSearchProduct);
productRouter.put("/", checkToken, putEditProduct);
productRouter.delete("/:id", checkToken, deleteProduct);

export default productRouter;
