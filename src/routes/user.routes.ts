import { Request, Response, NextFunction, Router } from "express";

import {
  getAllUsers,
  getUserById,
} from "../controllers/user.controllers";
import { checkToken } from "../middlewares";

const userRouter = Router();

userRouter.get("/", checkToken, getAllUsers);
userRouter.get("/:uid", checkToken, getUserById);

export default userRouter;
