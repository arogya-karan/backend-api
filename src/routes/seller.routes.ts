import { Router } from "express";

import {
  postAddSeller,
  getAllProducts,
  getSellerNearby,
  getAllProductsBySellerId,
  getSellerById,
  getSellerByUid,
} from "../controllers/seller.controllers";
import { checkToken } from "../middlewares";

let sellerRouter = Router();

sellerRouter.post("/addseller", checkToken, postAddSeller);
// sellerRouter.post("/addunverifiedseller", postAddUnverifiedSeller);

sellerRouter.get("/products", checkToken, getAllProducts);
sellerRouter.get("/productsbyid/:id", checkToken, getAllProductsBySellerId);
// sellerRouter.get("/products/:id", getAllProductsBySellerIdWithoutLogin);

sellerRouter.get("/nearby", checkToken, getSellerNearby);
// sellerRouter.get("/near", getSellerNearbyWithoutLogin);

sellerRouter.get("/:id", checkToken, getSellerById);
sellerRouter.get("/uid/:uid", checkToken, getSellerByUid);

export default sellerRouter;
