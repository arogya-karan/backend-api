import express from "express";
import mongoose from "mongoose";
import cors from "cors";
import morgan from "morgan";
import { config } from "dotenv";
import compression from "compression";
config();

// routes imports
import indexRoutes from "./routes/index.routes";
import userRoutes from "./routes/user.routes";
import authRoutes from "./routes/auth.routes";
import sellerRouter from "./routes/seller.routes";
import productRouter from "./routes/product.routes";
import transRouter from "./routes/transaction.routes";

console.log(process.env.MONGO_URL);

const MONGO_URI =
  process.env.NODE_ENV!.toString() == "production"
    ? (process.env.MONGO_URL as string)
    : (process.env.MONGO_URI as string);

const app = express();

// Bodyparser middlware
app.use(express.json());
// CORS Middleware
app.use(cors());
// Logger Middleware
app.use(morgan("dev"));

app.use(compression());

// mongoDB
mongoose
  .connect(MONGO_URI, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
  })
  .then(() => console.log("DB Connected!!"))
  .catch((err) => console.log(err));

// serve routes
app.use("/api", indexRoutes);
app.use("/api/user", userRoutes);
app.use("/api/auth", authRoutes);
app.use("/api/seller", sellerRouter);
app.use("/api/product", productRouter);
app.use("/api/trans", transRouter);

export default app;
