// import "./utils/tracer";
import app from "./app";
import { config } from "dotenv";
config();

app.listen(process.env.PORT, () =>
  console.log(`Server started on PORT ${process.env.PORT}`)
);
