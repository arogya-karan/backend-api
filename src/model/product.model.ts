import mongoose, { Document, Schema } from "mongoose";

interface IProduct extends Document {
  _id: any;
  productName: string;
  productCode: string;
  productDesc: string;
  productType: string;
  imageThumbnail: string;
  media: string[];
  productTags: string[];
  productPrice: number;
  quantity: number;
  availablity: boolean;
  isCertified: boolean;
  sellerId: string;
  dateOfIssue: string;
  geometry: {
    type: string;
    coordinates: number[];
  };
}

const geoSchema = new mongoose.Schema({
  type: {
    type: String,
    default: "Point",
  },
  coordinates: {
    type: [Number],
    index: "2dsphere",
  },
});

const productSchema = new Schema({
  productName: {
    type: String,
    required: true,
  },
  productDesc: {
    type: String,
    required: true,
  },
  productType: {
    type: String,
    required: true,
  },
  productCode: {
    type: String,
    required: true
  },
  productTags: [
    {
      type: String,
    },
  ],
  productPrice: {
    type: Number,
    required: true
  },
  media: [
    {
      type: String,
    },
  ],
  imageThumbnail: {
    type: String,
    default:
      "https://cdn0.iconfinder.com/data/icons/technology-business-and-people/1000/file_light-14-512.png",
  },
  quantity: {
    type: Number,
    required: true,
  },
  availablity: {
    type: Boolean,
    required: true,
  },
  isCertified: {
    type: Boolean,
    default: false,
  },
  sellerId: {
    type: String,
    required: true,
  },
  dateOfIssue: {
    type: String,
    required: true,
  },
  geometry: geoSchema,
});

productSchema.index({'$**': 'text'});


const ProductModel = mongoose.model<IProduct>("ProductModel", productSchema);

export default ProductModel;
