import mongoose, { Schema, Document } from "mongoose";

interface ISeller extends Document {
  _id: any;
  uid: string;
  sellerName: string;
  sellerEmail: string;
  sellerPhone: string;
  sellerAddress: string;
  isBusiness: boolean;
  geometry: {
    type: string;
    coordinates: number[];
  };
  productList: string[];
  isVerifiedSeller: boolean;
}

const geoSchema = new mongoose.Schema({
  type: {
    type: String,
    default: "Point",
  },
  coordinates: {
    type: [Number],
    index: "2dsphere",
  },
});

const sellerSchema = new Schema({
  uid: {
    type: String,
    required: true
  },
  sellerName: {
    type: String,
  },
  sellerEmail: {
    type: String,
  },
  isBusiness: {
    type: Boolean,
  },
  sellerPhone: {
    type: String,
  },
  sellerAddress: {
    type: String,
  },
  geometry: geoSchema,
  productList: [
    {
      type: String,
      required: true,
    },
  ],
  isVerifiedSeller: {
    type: Boolean,
    required: true
  }
});

const SellerModel = mongoose.model<ISeller>("SellerModel", sellerSchema);

export default SellerModel;
