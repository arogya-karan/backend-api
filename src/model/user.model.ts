import mongoose, { Document } from "mongoose";

interface IUser extends Document {
  _id: any;
  name: string;
  phone: number;
  email: string;
  password: string;
  geometry: {
    type: string;
    coordinates: number[];
  };
  address: string;
  isSeller: boolean;
  isVerified: boolean;
}

const geoSchema = new mongoose.Schema({
  type: {
    type: String,
    default: "Point",
  },
  coordinates: {
    type: [Number],
    index: "2dsphere",
  },
});

const userSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  phone: {
    type: String,
  },
  email: {
    type: String,
  },
  password: {
    type: String,
  },
  geometry: geoSchema,
  address: {
    type: String,
    required: true,
  },
  date: {
    type: String,
    default: Date.now(),
  },
  isSeller: {
    type: Boolean,
    default: false,
  },
  isVerified: {
    type: Boolean,
    required: true,
  },
});

const UserModel = mongoose.model<IUser>("User", userSchema);

export { UserModel, IUser };
