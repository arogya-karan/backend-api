import mongoose, { Document } from "mongoose";

interface ISellerTransaction extends Document {
  _id: any;
  sid: string;
  requested: [
    {
      quantity: number;
      pid: string;
      dateOfRequest: string;
      uid: String;
    }
  ];
  transactions: [
    {
      date: string;
      pid: string;
      uid: string;
      finalized: boolean;
      quantity: number;
    }
  ];
}

const sellerTransactionSchema = new mongoose.Schema({
  sid: { type: String, required: true },
  requested: [
    {
      "quantity": Number,
      "pid": String,
      "dateOfRequest": String,
      "uid": String,
    },
  ],
  transactions: [
    {
      "date": String,
      "pid": String,
      "uid": String,
      "finalized": Boolean,
      "quantity": Number,
    },
  ],
});

const STransactModel = mongoose.model<ISellerTransaction>(
  "Seller Transactions",
  sellerTransactionSchema
);

export { STransactModel, ISellerTransaction };
