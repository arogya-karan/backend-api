import mongoose, { Document } from "mongoose";

interface IUserTransaction extends Document {
  _id: any;
  uid: string;
  requested: [
    {
      quantity: number;
      pid: string;
      dateOfRequest: string;
      sid: String;
    }
  ];
  buy: [
    {
      date: string;
      pid: string;
      sid: string;
      finalized: boolean;
      quantity: number;
    }
  ];
}

const userTransactionSchema = new mongoose.Schema({
  uid: { type: String, required: true },
  requested: [
    {
      quantity: Number,
      pid: String,
      dateOfRequest: String,
      sid: String,
    },
  ],
  buy: [
    {
      date: String,
      pid: String,
      sid: String,
      finalized: Boolean,
      quantity: Number,
    },
  ],
});

const UTransactModel = mongoose.model<IUserTransaction>(
  "User Transactions",
  userTransactionSchema
);

export { UTransactModel, IUserTransaction };
